#cert-help

A helper which will let you find certificates on OSX

## Usage

**General Help**

```
$ cert-help --help
cert-help <command>
	-h --help              prints this help screen
	-v --version           show the current version
	search <term>          search for a certificate
	apple <method>         find apple certificates
```

**Command Specific Help**

```
$ cert-help search --help
cert-help search <term>
<term>   search for a particular term
```

##Development

###Dependencies

Dependencies are handled with __Bundler__ and stored in _Gemfile_.

```
bundle install
```

###Running in console

The _Rakefile_ contains a task for loading the project into __irb__

```
$ rake console
irb -rubygems -r ./lib/cert-help.rb
irb(main):001:0>
```

###Packaging Gem

Update `VERSION` in _lib/cert-help.rb_ with the correct new version
then run the __build__ task.

The task will update `s.files = %w[` with the contents of `git ls-files`,
meaning **files not added to git will not be packaged (few exceptions)**.

The resulting _gem_ will then be moved to the _pkg_ folder.

```
$ rake build
Updated cert-help.gemspec
mkdir -p pkg
gem build cert-help.gemspec
  Successfully built RubyGem
  Name: cert-help
  Version: 0.1
  File: cert-help-0.1.gem
mv cert-help-0.1.gem pkg
```
