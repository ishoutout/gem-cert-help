begin
  require 'rubygems'
rescue LoadError
end

$:.unshift File.join(File.dirname(__FILE__), *%w[.. lib])

require 'cert-help/apple'
require 'cert-help/command'
require 'cert-help/search'

module Certs
  VERSION = "0.1"
  COMMAND = 'cert-help'
end
