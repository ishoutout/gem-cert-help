module Certs
  class Apple
    class << self

      def execute(command)

        return apple(command) if ['dev', 'dist', 'all'].includes?(command)
        return help

      end

      def apple(command)

        certs = []

        searches = []

        searches << "iPhone Distribution" if command == 'dist' || command == 'all'
        searches << "iPhone Developer"    if command == 'dev' || command == 'all'

        searches.each do |term|
          Certs::Search.searchAux(certs, term)
        end

        if certs.size > 0
          puts certs
        else
          puts "Couldn't find any certs"
        end

      end

      def help
        puts "#{Certs::COMMAND} apple <command>"
        puts "<command> is optional"
        puts "\tdist    search for distribution certificates"
        puts "\tdev     search for development certificates"
      end

    end
  end
end
