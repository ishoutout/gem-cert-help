module Certs
  class Search
    class << self

      def execute(command)

        return help if command.nil? || ['help', '-h', '--help'].include?(command)
        return certs(command)

      end

      def help
        puts "#{Certs::COMMAND} search <term>"
        puts "<term>   search for a particular term"
      end

      def certs(term)

        certs = []
        searchAux(certs, term)

        if certs.size > 0
          puts certs
        else
          puts "Couldn't find any certs"
        end

      end

      def searchAux(certs, term)

        results = `security find-certificate -a -c "#{term}"`

        match = /"alis"<blob>="(.*)"$/.match(results)

        return false if match.nil?

        match.captures.each do |c|
          certs << c
        end

      end

    end
  end
end
