module Certs

  # Provides terminal access to the ruby api
  #
  # Deligates commands based on user input, displaying
  # version, help, and listing certificates based on searches
  #
  # @author Nick Otter <notter@ishoutout.com>
  class Command
    class << self

      # Terns ARGV to marjor, minor commands
      #
      # @param [Array] *args The user input
      def execute(*args)

        major = args.shift
        minor = args.shift

        deligate(major, minor)

      end

      # Decides which command to execute based on major, minor
      def deligate(major, minor)
        return version if ['-v', '--version'].include?(major)

        return Certs::Apple.execute(minor) if major == 'apple'
        return Certs::Search.execute(minor) if major == 'search'

        help
      end

      # Informs the user how to use this program
      def help
        puts "#{Certs::COMMAND} <command>"
        puts "\t-h --help              prints this help screen"
        puts "\t-v --version           show the current version"
        puts "\tsearch <term>          search for a certificate"
        puts "\tapple <method>         find apple certificates"
      end

      # Shows the user which version is being used
      def version
        puts "Version: #{Certs::VERSION}"
      end

    end
  end
end
